tinymce.PluginManager.add('plugin_edu.msu.anr.tinymce_blockquote', function(editor) {

    var selectedNode;

    function createBlockQuote(){
        selectedNode = editor.selection.getNode();
        var title = 'Add BlockQuote';
        var value = 'Place BlockQuote Here';
        var citation = '';
        switch (selectedNode.tagName){
          case 'BODY':
            selectedNode = selectedNode.firstChild;
            break;
          case 'CITE':
            selectedNode = selectedNode.parentNode;
            break;
          case 'BLOCKQUOTE':
            break;
          default:
            if(selectedNode.textContent.trim() !== ''){
                value = selectedNode.textContent.trim();
            }
        }
        if(selectedNode.tagName == 'P' && selectedNode.textContent.trim() !== ''){
            value = selectedNode.textContent.trim();
        }

        if(selectedNode.tagName == 'BLOCKQUOTE'){
            title = 'Edit BlockQuote';
            var children = selectedNode.childNodes;
            Array.from(children).forEach(function(element, index){
                if(index == 0){
                    value = element.textContent.trim();
                }
                if(element.tagName == 'CITE'){
                    citation = element.textContent.trim();
                }
            });
        }

        editor.windowManager.open({
            title: title,
            body: [
                {
                    type: 'textbox',
                    name: 'quotetext',
                    label: 'Blockquote',
                    value: value
                },
                {
                    type: 'textbox',
                    name: 'citationtext',
                    label: 'Citation (optional)',
                    value: citation
                }
            ],
            onsubmit: function(e) {
                let quotetext = e.data.quotetext;
                let citationtext = e.data.citationtext;
                if (quotetext.trim() !== '' && citationtext.trim() !== '') {
                    insertQuote(quotetext, citationtext);
                } else if(quotetext.trim() !== ''){
                    insertQuote(quotetext, false);
                }
            }
        });
    }

    function insertQuote(quotetext, citationtext){
        var citationElement = "";
        if(citationtext){
            citationElement = editor.dom.createHTML('cite', null, citationtext);
        }

        editor.selection.select(selectedNode);
        editor.selection.setContent(editor.dom.createHTML('blockquote', null, quotetext + ' ' + citationElement));
        var endId = tinymce.DOM.uniqueId();
        editor.dom.add(editor.getBody(), 'span', {'id':endId},'&nbsp;');
        var newNode = editor.dom.select('span#'+endId);
        editor.selection.select(newNode[0]);
    }

    // Add a button that opens a window
    editor.addButton('blockquote', {
        icon: 'blockquote',
        tooltip: 'Insert Blockquote',
        onclick: createBlockQuote,
        stateSelector: 'blockquote,cite'
    });

    // Adds a menu item to the tools menu
    editor.addMenuItem('blockquote', {
        icon: 'blockquote',
        tooltip: 'Insert Blockquote',
        onclick: createBlockQuote,
        context: 'insert',
    });

});
