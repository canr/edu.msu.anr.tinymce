tinymce.PluginManager.add('plugin_edu.msu.anr.tinymce_youtube2', function(editor) {

    function openInsertWindow() {
        let selectedNode = editor.selection.getNode();
        let videoId = '';
        var responsive = false;

        // Get current ID if cursor's on a YouTube video
        if (isYoutubeNode(selectedNode)) {
            videoId = parseVideoId(selectedNode.getAttribute('data-mce-p-src'));
            responsive = (selectedNode.getAttribute('data-mce-p-data-isresponsive') === 'true');
        }

        editor.windowManager.open({
            title: 'Insert/Edit YouTube Video',
            body: [
                {
                    type: 'textbox',
                    name: 'videoUrlOrCode',
                    label: 'Video URL or Code',
                    value: videoId
                },
                {name: 'responsive', type: 'checkbox', checked: responsive, text: 'Make Responsive'},
            ],
            onsubmit: function(e) {
                let videoId = parseVideoId(e.data.videoUrlOrCode);
                isresponsive = e.data.responsive;
                if (videoId) {
                    insertVideo(videoId, isresponsive);
                }
            }
        });
    }

    function isYoutubeNode(node) {
        let matchTag = node.tagName == 'IMG';
        let srcSet = matchTag
            && node.getAttribute('data-mce-p-src') !== null
            && node.getAttribute('data-mce-p-src') !== "";
        let srcYoutube = srcSet
            && (
                node.getAttribute('data-mce-p-src').includes('youtu.be')
                || node.getAttribute('data-mce-p-src').includes('youtube.com'));

        return matchTag && srcSet && srcYoutube;
    }

    function parseVideoId(urlOrId) {
        const ID_LENGTH = 11;
        let matches = urlOrId.match(/^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\??v?=?))([^#\&\?]*).*/);

        if ( matches != null && matches[7].length == ID_LENGTH ) { // Code extracted from URL
            return matches[7];
        }
        else {
            let idRegex = new RegExp('^[a-zA-Z0-9]{'+ID_LENGTH+'}$');
            if ( idRegex.test(urlOrId) ) { // Code given
                return urlOrId;
            }
            return false;
        }
    }

    function insertVideo(videoId, isresponsive) {
        let selectedNode = editor.selection.getNode();
        console.log(selectedNode);
        videoElement = editor.dom.createHTML('iframe', {width: "560", height: "315", src: "https://www.youtube.com/embed/"+videoId, frameborder:"0", allowfullscreen: "true", 'data-isResponsive': isresponsive},'');

        if(isresponsive){
            editor.insertContent(editor.dom.createHTML('div', {class: 'responsive-embed widescreen'}, videoElement));
        } else {
            if(selectedNode.parentNode.classList.contains('responsive-embed')){
                editor.selection.select(selectedNode.parentNode);
                editor.selection.setContent(editor.dom.createHTML('p'));
            }
            editor.insertContent(videoElement);
        }
        var endId = tinymce.DOM.uniqueId();
        editor.dom.add(editor.getBody(), 'span', {'id':endId},'&nbsp;');
        var newNode = editor.dom.select('span#'+endId);
        editor.selection.select(newNode[0]);
    }

    // Add a button that opens a window
    editor.addButton('youtube2', {
        image: 'https://www.youtube.com/yts/img/favicon_144-vflWmzoXw.png',
        tooltip: 'Insert YouTube Video',
        onclick: openInsertWindow,
        stateSelector: 'div.responsive-embed,iframe,mce-object-iframe'
    });

    // Adds a menu item to the tools menu
    editor.addMenuItem('youtube2', {
        text: 'YouTube Video',
        tooltip: 'Insert YouTube Video',
        onclick: openInsertWindow,
        context: 'insert',
    });

});
