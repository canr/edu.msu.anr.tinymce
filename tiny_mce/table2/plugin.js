'use strict';
!function(elem, undefined) {
  /**
   * @param {!Array} data
   * @param {!Function} callback
   * @return {undefined}
   */
  function b(data, callback) {
    var c;
    /** @type {!Array} */
    var d = [];
    /** @type {number} */
    var i = 0;
    for (; i < data.length; ++i) {
      if (c = memo[data[i]] || e(data[i]), !c) {
        throw "module definition dependecy not found: " + data[i];
      }
      d.push(c);
    }
    callback.apply(null, d);
  }
  /**
   * @param {string} id
   * @param {!Array} f
   * @param {!Function} fallback
   * @return {undefined}
   */
  function define(id, f, fallback) {
    if ("string" != typeof id) {
      throw "invalid module definition, module id must be defined and be a string";
    }
    if (f === undefined) {
      throw "invalid module definition, dependencies must be specified";
    }
    if (fallback === undefined) {
      throw "invalid module definition, definition function must be specified";
    }
    b(f, function() {
      memo[id] = fallback.apply(null, arguments);
    });
  }
  /**
   * @param {string} type
   * @return {?}
   */
  function e(type) {
    /** @type {!global this} */
    var node = elem;
    var keyMatches = type.split(/[.\/]/);
    /** @type {number} */
    var i = 0;
    for (; i < keyMatches.length; ++i) {
      if (!node[keyMatches[i]]) {
        return;
      }
      node = node[keyMatches[i]];
    }
    return node;
  }
  var memo = {};
  define("tinymce/tableplugin/TableGrid", ["tinymce/util/Tools", "tinymce/Env"], function(Tools, Env) {
    /**
     * @param {!Object} td
     * @param {string} name
     * @return {?}
     */
    function getSpanVal(td, name) {
      return parseInt(td.getAttribute(name) || 1, 10);
    }
    var each = Tools.each;
    return function(editor, table) {
      /**
       * @return {undefined}
       */
      function buildGrid() {
        /** @type {number} */
        var offset = 0;
        /** @type {!Array} */
        grid = [];
        /** @type {number} */
        gridWidth = 0;
        each(["thead", "tbody", "tfoot"], function(trackId) {
          var rows = dom.select("> " + trackId + " tr", table);
          each(rows, function(tr, y) {
            y = y + offset;
            each(dom.select("> td, > th", tr), function(td, x) {
              var x2;
              var y2;
              var rowspan;
              var colspan;
              if (grid[y]) {
                for (; grid[y][x];) {
                  x++;
                }
              }
              rowspan = getSpanVal(td, "rowspan");
              colspan = getSpanVal(td, "colspan");
              y2 = y;
              for (; y + rowspan > y2; y2++) {
                if (!grid[y2]) {
                  /** @type {!Array} */
                  grid[y2] = [];
                }
                /** @type {number} */
                x2 = x;
                for (; x + colspan > x2; x2++) {
                  grid[y2][x2] = {
                    part : trackId,
                    real : y2 == y && x2 == x,
                    elm : td,
                    rowspan : rowspan,
                    colspan : colspan
                  };
                }
              }
              /** @type {number} */
              gridWidth = Math.max(gridWidth, x + 1);
            });
          });
          offset = offset + rows.length;
        });
      }
      /**
       * @param {!Object} node
       * @param {boolean} children
       * @return {?}
       */
      function cloneNode(node, children) {
        return node = node.cloneNode(children), node.removeAttribute("id"), node;
      }
      /**
       * @param {number} x
       * @param {number} y
       * @return {?}
       */
      function getCell(x, y) {
        var row;
        return row = grid[y], row ? row[x] : void 0;
      }
      /**
       * @param {!Object} td
       * @param {string} name
       * @param {number} value
       * @return {undefined}
       */
      function setSpanVal(td, name, value) {
        if (td) {
          /** @type {number} */
          value = parseInt(value, 10);
          if (1 === value) {
            td.removeAttribute(name, 1);
          } else {
            td.setAttribute(name, value, 1);
          }
        }
      }
      /**
       * @param {!Object} cell
       * @return {?}
       */
      function isCellSelected(cell) {
        return cell && (dom.hasClass(cell.elm, "mce-item-selected") || cell == selectedCell);
      }
      /**
       * @return {?}
       */
      function getSelectedRows() {
        /** @type {!Array} */
        var new_broadcasts = [];
        return each(table.rows, function(b) {
          each(b.cells, function(cell) {
            return dom.hasClass(cell, "mce-item-selected") || selectedCell && cell == selectedCell.elm ? (new_broadcasts.push(b), false) : void 0;
          });
        }), new_broadcasts;
      }
      /**
       * @return {undefined}
       */
      function deleteTable() {
        var rng = dom.createRng();
        rng.setStartAfter(table);
        rng.setEndAfter(table);
        selection.setRng(rng);
        dom.remove(table);
      }
      /**
       * @param {!Object} cell
       * @return {?}
       */
      function cloneCell(cell) {
        var f;
        var out = {};
        return editor.settings.table_clone_elements !== false && (out = Tools.makeMap((editor.settings.table_clone_elements || "strong em b i span font h1 h2 h3 h4 h5 h6 p div").toUpperCase(), /[ ,]/)), Tools.walk(cell, function(e) {
          var t;
          return 3 == e.nodeType ? (each(dom.getParents(e.parentNode, null, cell).reverse(), function(cell) {
            if (out[cell.nodeName]) {
              cell = cloneNode(cell, false);
              if (f) {
                if (t) {
                  t.appendChild(cell);
                }
              } else {
                f = t = cell;
              }
              /** @type {!Object} */
              t = cell;
            }
          }), t && (t.innerHTML = Env.ie ? "&nbsp;" : '<br data-mce-bogus="1" />'), false) : void 0;
        }, "childNodes"), cell = cloneNode(cell, false), setSpanVal(cell, "rowSpan", 1), setSpanVal(cell, "colSpan", 1), f ? cell.appendChild(f) : (!Env.ie || Env.ie > 10) && (cell.innerHTML = '<br data-mce-bogus="1" />'), cell;
      }
      /**
       * @return {?}
       */
      function cleanup() {
        var row;
        var rng = dom.createRng();
        return each(dom.select("tr", table), function(row) {
          if (0 === row.cells.length) {
            dom.remove(row);
          }
        }), 0 === dom.select("tr", table).length ? (rng.setStartBefore(table), rng.setEndBefore(table), selection.setRng(rng), void dom.remove(table)) : (each(dom.select("thead,tbody,tfoot", table), function(a) {
          if (0 === a.rows.length) {
            dom.remove(a);
          }
        }), buildGrid(), void(startPos && (row = grid[Math.min(grid.length - 1, startPos.y)], row && (selection.select(row[Math.min(row.length - 1, startPos.x)].elm, true), selection.collapse(true)))));
      }
      /**
       * @param {number} x
       * @param {number} y
       * @param {number} rows
       * @param {number} cols
       * @return {undefined}
       */
      function fillLeftDown(x, y, rows, cols) {
        var tr;
        var x2;
        var newOffsetY;
        var max;
        var cell;
        tr = grid[y][x].elm.parentNode;
        /** @type {number} */
        newOffsetY = 1;
        for (; rows >= newOffsetY; newOffsetY++) {
          if (tr = dom.getNext(tr, "tr")) {
            /** @type {number} */
            x2 = x;
            for (; x2 >= 0; x2--) {
              if (cell = grid[y + newOffsetY][x2].elm, cell.parentNode == tr) {
                /** @type {number} */
                max = 1;
                for (; cols >= max; max++) {
                  dom.insertAfter(cloneCell(cell), cell);
                }
                break;
              }
            }
            if (-1 == x2) {
              /** @type {number} */
              max = 1;
              for (; cols >= max; max++) {
                tr.insertBefore(cloneCell(tr.cells[0]), tr.cells[0]);
              }
            }
          }
        }
      }
      /**
       * @return {undefined}
       */
      function split() {
        each(grid, function(concatModules, y) {
          each(concatModules, function(cell, x) {
            var colSpan;
            var rowSpan;
            var g;
            if (isCellSelected(cell) && (cell = cell.elm, colSpan = getSpanVal(cell, "colspan"), rowSpan = getSpanVal(cell, "rowspan"), colSpan > 1 || rowSpan > 1)) {
              setSpanVal(cell, "rowSpan", 1);
              setSpanVal(cell, "colSpan", 1);
              /** @type {number} */
              g = 0;
              for (; colSpan - 1 > g; g++) {
                dom.insertAfter(cloneCell(cell), cell);
              }
              fillLeftDown(x, y, rowSpan - 1, colSpan);
            }
          });
        });
      }
      /**
       * @param {string} cell
       * @param {number} cols
       * @param {number} rows
       * @return {undefined}
       */
      function merge(cell, cols, rows) {
        var pos;
        var startX;
        var startY;
        var endX;
        var endY;
        var x;
        var y;
        var startCell;
        var endCell;
        var exportDivElements;
        var v;
        if (cell ? (pos = getPos(cell), startX = pos.x, startY = pos.y, endX = startX + (cols - 1), endY = startY + (rows - 1)) : (startPos = endPos = null, each(grid, function(concatModules, fixedMapY) {
          each(concatModules, function(cell, fixedMapX) {
            if (isCellSelected(cell)) {
              if (!startPos) {
                startPos = {
                  x : fixedMapX,
                  y : fixedMapY
                };
              }
              endPos = {
                x : fixedMapX,
                y : fixedMapY
              };
            }
          });
        }), startPos && (startX = startPos.x, startY = startPos.y, endX = endPos.x, endY = endPos.y)), startCell = getCell(startX, startY), endCell = getCell(endX, endY), startCell && endCell && startCell.part == endCell.part) {
          split();
          buildGrid();
          startCell = getCell(startX, startY).elm;
          setSpanVal(startCell, "colSpan", endX - startX + 1);
          setSpanVal(startCell, "rowSpan", endY - startY + 1);
          y = startY;
          for (; endY >= y; y++) {
            x = startX;
            for (; endX >= x; x++) {
              if (grid[y] && grid[y][x]) {
                cell = grid[y][x].elm;
                if (cell != startCell) {
                  exportDivElements = Tools.grep(cell.childNodes);
                  each(exportDivElements, function(drag) {
                    startCell.appendChild(drag);
                  });
                  if (exportDivElements.length) {
                    exportDivElements = Tools.grep(startCell.childNodes);
                    /** @type {number} */
                    v = 0;
                    each(exportDivElements, function(node) {
                      if ("BR" == node.nodeName && dom.getAttrib(node, "data-mce-bogus") && v++ < exportDivElements.length - 1) {
                        startCell.removeChild(node);
                      }
                    });
                  }
                  dom.remove(cell);
                }
              }
            }
          }
          cleanup();
        }
      }
      /**
       * @param {string} afterOrBefore
       * @return {undefined}
       */
      function insertRow(afterOrBefore) {
        var posY;
        var cell;
        var lastCell;
        var x;
        var rowElm;
        var newRow;
        var newCell;
        var otherCell;
        var rowSpan;
        if (each(grid, function(concatModules, bottom) {
          return each(concatModules, function(cell) {
            return isCellSelected(cell) && (cell = cell.elm, rowElm = cell.parentNode, newRow = cloneNode(rowElm, false), posY = bottom, afterOrBefore) ? false : void 0;
          }), afterOrBefore ? !posY : void 0;
        }), posY !== undefined) {
          /** @type {number} */
          x = 0;
          for (; x < grid[0].length; x++) {
            if (grid[posY][x] && (cell = grid[posY][x].elm, cell != lastCell)) {
              if (afterOrBefore) {
                if (posY > 0 && grid[posY - 1][x] && (otherCell = grid[posY - 1][x].elm, rowSpan = getSpanVal(otherCell, "rowSpan"), rowSpan > 1)) {
                  setSpanVal(otherCell, "rowSpan", rowSpan + 1);
                  continue;
                }
              } else {
                if (rowSpan = getSpanVal(cell, "rowspan"), rowSpan > 1) {
                  setSpanVal(cell, "rowSpan", rowSpan + 1);
                  continue;
                }
              }
              newCell = cloneCell(cell);
              setSpanVal(newCell, "colSpan", cell.colSpan);
              newRow.appendChild(newCell);
              lastCell = cell;
            }
          }
          if (newRow.hasChildNodes()) {
            if (afterOrBefore) {
              rowElm.parentNode.insertBefore(newRow, rowElm);
            } else {
              dom.insertAfter(newRow, rowElm);
            }
          }
        }
      }
      /**
       * @param {string} isNot
       * @return {undefined}
       */
      function insertCol(isNot) {
        var x;
        var lastCell;
        each(grid, function(concatModules) {
          return each(concatModules, function(cell, stripTo) {
            return isCellSelected(cell) && (x = stripTo, isNot) ? false : void 0;
          }), isNot ? !x : void 0;
        });
        each(grid, function(row, y) {
          var cell;
          var rowSpan;
          var colSpan;
          if (row[x]) {
            cell = row[x].elm;
            if (cell != lastCell) {
              colSpan = getSpanVal(cell, "colspan");
              rowSpan = getSpanVal(cell, "rowspan");
              if (1 == colSpan) {
                if (isNot) {
                  cell.parentNode.insertBefore(cloneCell(cell), cell);
                  fillLeftDown(x, y, rowSpan - 1, colSpan);
                } else {
                  dom.insertAfter(cloneCell(cell), cell);
                  fillLeftDown(x, y, rowSpan - 1, colSpan);
                }
              } else {
                setSpanVal(cell, "colSpan", cell.colSpan + 1);
              }
              lastCell = cell;
            }
          }
        });
      }
      /**
       * @return {undefined}
       */
      function deleteCols() {
        /** @type {!Array} */
        var level = [];
        each(grid, function(concatModules) {
          each(concatModules, function(cell, index) {
            if (isCellSelected(cell) && -1 === Tools.inArray(level, index)) {
              each(grid, function(row) {
                var colSpan;
                var cell = row[index].elm;
                colSpan = getSpanVal(cell, "colSpan");
                if (colSpan > 1) {
                  setSpanVal(cell, "colSpan", colSpan - 1);
                } else {
                  dom.remove(cell);
                }
              });
              level.push(index);
            }
          });
        });
        cleanup();
      }
      /**
       * @return {undefined}
       */
      function deleteRows() {
        /**
         * @param {!Object} item
         * @return {undefined}
         */
        function deleteRow(item) {
          var pos;
          var lastCell;
          each(item.cells, function(cell) {
            var rowSpan = getSpanVal(cell, "rowSpan");
            if (rowSpan > 1) {
              setSpanVal(cell, "rowSpan", rowSpan - 1);
              pos = getPos(cell);
              fillLeftDown(pos.x, pos.y, 1, 1);
            }
          });
          pos = getPos(item.cells[0]);
          each(grid[pos.y], function(cell) {
            var rowSpan;
            cell = cell.elm;
            if (cell != lastCell) {
              rowSpan = getSpanVal(cell, "rowSpan");
              if (1 >= rowSpan) {
                dom.remove(cell);
              } else {
                setSpanVal(cell, "rowSpan", rowSpan - 1);
              }
              /** @type {string} */
              lastCell = cell;
            }
          });
        }
        var rows;
        rows = getSelectedRows();
        each(rows.reverse(), function(tr) {
          deleteRow(tr);
        });
        cleanup();
      }
      /**
       * @return {?}
       */
      function cutRows() {
        var rows = getSelectedRows();
        return dom.remove(rows), cleanup(), rows;
      }
      /**
       * @return {?}
       */
      function copyRows() {
        var rows = getSelectedRows();
        return each(rows, function(row, i) {
          rows[i] = cloneNode(row, true);
        }), rows;
      }
      /**
       * @param {!Array} rows
       * @param {boolean} before
       * @return {undefined}
       */
      function pasteRows(rows, before) {
        var selectedRows = getSelectedRows();
        var targetRow = selectedRows[before ? 0 : selectedRows.length - 1];
        var length = targetRow.cells.length;
        if (rows) {
          each(grid, function(testDirPaths) {
            var b;
            return length = 0, each(testDirPaths, function(item) {
              if (item.real) {
                length = length + item.colspan;
              }
              if (item.elm.parentNode == targetRow) {
                /** @type {number} */
                b = 1;
              }
            }), b ? false : void 0;
          });
          if (!before) {
            rows.reverse();
          }
          each(rows, function(row) {
            var index;
            var cell;
            var i = row.cells.length;
            /** @type {number} */
            index = 0;
            for (; i > index; index++) {
              cell = row.cells[index];
              setSpanVal(cell, "colSpan", 1);
              setSpanVal(cell, "rowSpan", 1);
            }
            index = i;
            for (; length > index; index++) {
              row.appendChild(cloneCell(row.cells[i - 1]));
            }
            index = length;
            for (; i > index; index++) {
              dom.remove(row.cells[index]);
            }
            if (before) {
              targetRow.parentNode.insertBefore(row, targetRow);
            } else {
              dom.insertAfter(row, targetRow);
            }
          });
          dom.removeClass(dom.select("td.mce-item-selected,th.mce-item-selected"), "mce-item-selected");
        }
      }
      /**
       * @param {!Object} target
       * @return {?}
       */
      function getPos(target) {
        var currentShadingPosition;
        return each(grid, function(concatModules, fixedMapY) {
          return each(concatModules, function(cell, fixedMapX) {
            return cell.elm == target ? (currentShadingPosition = {
              x : fixedMapX,
              y : fixedMapY
            }, false) : void 0;
          }), !currentShadingPosition;
        }), currentShadingPosition;
      }
      /**
       * @param {!Object} cell
       * @return {undefined}
       */
      function setStartCell(cell) {
        startPos = getPos(cell);
      }
      /**
       * @return {?}
       */
      function findEndPos() {
        var end;
        var maxY;
        return end = maxY = 0, each(grid, function(concatModules, y) {
          each(concatModules, function(cell, i) {
            var offset;
            var rowSpan;
            if (isCellSelected(cell)) {
              cell = grid[y][i];
              if (i > end) {
                /** @type {number} */
                end = i;
              }
              if (y > maxY) {
                /** @type {number} */
                maxY = y;
              }
              if (cell.real) {
                /** @type {number} */
                offset = cell.colspan - 1;
                /** @type {number} */
                rowSpan = cell.rowspan - 1;
                if (offset && i + offset > end) {
                  end = i + offset;
                }
                if (rowSpan && y + rowSpan > maxY) {
                  maxY = y + rowSpan;
                }
              }
            }
          });
        }), {
          x : end,
          y : maxY
        };
      }
      /**
       * @param {!Object} cell
       * @return {undefined}
       */
      function setEndCell(cell) {
        var startX;
        var j;
        var k;
        var t;
        var value;
        var v;
        var minValue;
        var _c;
        var i;
        var p;
        if (endPos = getPos(cell), startPos && endPos) {
          /** @type {number} */
          startX = Math.min(startPos.x, endPos.x);
          /** @type {number} */
          j = Math.min(startPos.y, endPos.y);
          /** @type {number} */
          k = Math.max(startPos.x, endPos.x);
          /** @type {number} */
          t = Math.max(startPos.y, endPos.y);
          /** @type {number} */
          value = k;
          /** @type {number} */
          v = t;
          /** @type {number} */
          p = j;
          for (; v >= p; p++) {
            cell = grid[p][startX];
            if (!cell.real) {
              if (startX - (cell.colspan - 1) < startX) {
                /** @type {number} */
                startX = startX - (cell.colspan - 1);
              }
            }
          }
          /** @type {number} */
          i = startX;
          for (; value >= i; i++) {
            cell = grid[j][i];
            if (!cell.real) {
              if (j - (cell.rowspan - 1) < j) {
                /** @type {number} */
                j = j - (cell.rowspan - 1);
              }
            }
          }
          /** @type {number} */
          p = j;
          for (; t >= p; p++) {
            /** @type {number} */
            i = startX;
            for (; k >= i; i++) {
              cell = grid[p][i];
              if (cell.real) {
                /** @type {number} */
                minValue = cell.colspan - 1;
                /** @type {number} */
                _c = cell.rowspan - 1;
                if (minValue && i + minValue > value) {
                  /** @type {number} */
                  value = i + minValue;
                }
                if (_c && p + _c > v) {
                  /** @type {number} */
                  v = p + _c;
                }
              }
            }
          }
          dom.removeClass(dom.select("td.mce-item-selected,th.mce-item-selected"), "mce-item-selected");
          /** @type {number} */
          p = j;
          for (; v >= p; p++) {
            /** @type {number} */
            i = startX;
            for (; value >= i; i++) {
              if (grid[p][i]) {
                dom.addClass(grid[p][i].elm, "mce-item-selected");
              }
            }
          }
        }
      }
      /**
       * @param {!Object} cellElm
       * @param {string} delta
       * @return {?}
       */
      function moveRelIdx(cellElm, delta) {
        var pos;
        var index;
        var cell;
        pos = getPos(cellElm);
        index = pos.y * gridWidth + pos.x;
        do {
          if (index = index + delta, cell = getCell(index % gridWidth, Math.floor(index / gridWidth)), !cell) {
            break;
          }
          if (cell.elm != cellElm) {
            return selection.select(cell.elm, true), dom.isEmpty(cell.elm) && selection.collapse(true), true;
          }
        } while (cell.elm == cellElm);
        return false;
      }
      var grid;
      var gridWidth;
      var startPos;
      var endPos;
      var selectedCell;
      var selection = editor.selection;
      var dom = selection.dom;
      table = table || dom.getParent(selection.getStart(), "table");
      buildGrid();
      selectedCell = dom.getParent(selection.getStart(), "th,td");
      if (selectedCell) {
        startPos = getPos(selectedCell);
        endPos = findEndPos();
        selectedCell = getCell(startPos.x, startPos.y);
      }
      Tools.extend(this, {
        deleteTable : deleteTable,
        split : split,
        merge : merge,
        insertRow : insertRow,
        insertCol : insertCol,
        deleteCols : deleteCols,
        deleteRows : deleteRows,
        cutRows : cutRows,
        copyRows : copyRows,
        pasteRows : pasteRows,
        getPos : getPos,
        setStartCell : setStartCell,
        setEndCell : setEndCell,
        moveRelIdx : moveRelIdx,
        refresh : buildGrid
      });
    };
  });
  define("tinymce/tableplugin/Quirks", ["tinymce/util/VK", "tinymce/Env", "tinymce/util/Tools"], function(keys, Env, Tools) {
    /**
     * @param {!Node} td
     * @param {string} name
     * @return {?}
     */
    function getSpanVal(td, name) {
      return parseInt(td.getAttribute(name) || 1, 10);
    }
    var each = Tools.each;
    return function(editor) {
      /**
       * @return {undefined}
       */
      function handleContentEditableSelection() {
        /**
         * @param {!Event} event
         * @return {undefined}
         */
        function moveSelection(event) {
          /**
           * @param {boolean} upBool
           * @param {?} sourceNode
           * @return {?}
           */
          function handle(upBool, sourceNode) {
            /** @type {string} */
            var siblingDirection = upBool ? "previousSibling" : "nextSibling";
            var currentRow = editor.dom.getParent(sourceNode, "tr");
            var siblingRow = currentRow[siblingDirection];
            if (siblingRow) {
              return moveCursorToRow(editor, sourceNode, siblingRow, upBool), event.preventDefault(), true;
            }
            var tableNode = editor.dom.getParent(currentRow, "table");
            var middleNode = currentRow.parentNode;
            var parentNodeName = middleNode.nodeName.toLowerCase();
            if ("tbody" === parentNodeName || parentNodeName === (upBool ? "tfoot" : "thead")) {
              var targetParent = getTargetParent(upBool, tableNode, middleNode, "tbody");
              if (null !== targetParent) {
                return moveToRowInTarget(upBool, targetParent, sourceNode);
              }
            }
            return escapeTable(upBool, currentRow, siblingDirection, tableNode);
          }
          /**
           * @param {boolean} upBool
           * @param {?} topNode
           * @param {!Node} secondNode
           * @param {string} nodeName
           * @return {?}
           */
          function getTargetParent(upBool, topNode, secondNode, nodeName) {
            var tbodies = editor.dom.select(">" + nodeName, topNode);
            var position = tbodies.indexOf(secondNode);
            if (upBool && 0 === position || !upBool && position === tbodies.length - 1) {
              return getFirstHeadOrFoot(upBool, topNode);
            }
            if (-1 === position) {
              /** @type {number} */
              var topOrBottom = "thead" === secondNode.tagName.toLowerCase() ? 0 : tbodies.length - 1;
              return tbodies[topOrBottom];
            }
            return tbodies[position + (upBool ? -1 : 1)];
          }
          /**
           * @param {string} upBool
           * @param {?} parent
           * @return {?}
           */
          function getFirstHeadOrFoot(upBool, parent) {
            /** @type {string} */
            var tagName = upBool ? "thead" : "tfoot";
            var $videoThumb = editor.dom.select(">" + tagName, parent);
            return 0 !== $videoThumb.length ? $videoThumb[0] : null;
          }
          /**
           * @param {string} upBool
           * @param {!Object} targetParent
           * @param {?} sourceNode
           * @return {?}
           */
          function moveToRowInTarget(upBool, targetParent, sourceNode) {
            var targetRow = getChildForDirection(targetParent, upBool);
            return targetRow && moveCursorToRow(editor, sourceNode, targetRow, upBool), event.preventDefault(), true;
          }
          /**
           * @param {boolean} upBool
           * @param {!Object} currentRow
           * @param {string} siblingDirection
           * @param {!Object} table
           * @return {?}
           */
          function escapeTable(upBool, currentRow, siblingDirection, table) {
            var tableSibling = table[siblingDirection];
            if (tableSibling) {
              return moveCursorToStartOfElement(tableSibling), true;
            }
            var parentCell = editor.dom.getParent(table, "td,th");
            if (parentCell) {
              return handle(upBool, parentCell, event);
            }
            var backUpSibling = getChildForDirection(currentRow, !upBool);
            return moveCursorToStartOfElement(backUpSibling), event.preventDefault(), false;
          }
          /**
           * @param {!Object} parent
           * @param {string} up
           * @return {?}
           */
          function getChildForDirection(parent, up) {
            var node = parent && parent[up ? "lastChild" : "firstChild"];
            return node && "BR" === node.nodeName ? editor.dom.getParent(node, "td,th") : node;
          }
          /**
           * @param {?} n
           * @return {undefined}
           */
          function moveCursorToStartOfElement(n) {
            editor.selection.setCursorLocation(n, 0);
          }
          /**
           * @return {?}
           */
          function isVerticalMovement() {
            return key == keys.UP || key == keys.DOWN;
          }
          /**
           * @param {!Object} editor
           * @return {?}
           */
          function isInTable(editor) {
            var win = editor.selection.getNode();
            var sel = editor.dom.getParent(win, "tr");
            return null !== sel;
          }
          /**
           * @param {string} column
           * @return {?}
           */
          function columnIndex(column) {
            /** @type {number} */
            var colIndex = 0;
            /** @type {string} */
            var c = column;
            for (; c.previousSibling;) {
              c = c.previousSibling;
              colIndex = colIndex + getSpanVal(c, "colspan");
            }
            return colIndex;
          }
          /**
           * @param {!Node} rowElement
           * @param {number} columnIndex
           * @return {?}
           */
          function findColumn(rowElement, columnIndex) {
            /** @type {number} */
            var c = 0;
            /** @type {number} */
            var s = 0;
            return each(rowElement.children, function(cell, none) {
              return c = c + getSpanVal(cell, "colspan"), s = none, c > columnIndex ? false : void 0;
            }), s;
          }
          /**
           * @param {!Object} ed
           * @param {?} node
           * @param {!Node} row
           * @param {string} upBool
           * @return {undefined}
           */
          function moveCursorToRow(ed, node, row, upBool) {
            var srcColumnIndex = columnIndex(editor.dom.getParent(node, "td,th"));
            var tgtColumnIndex = findColumn(row, srcColumnIndex);
            var tgtNode = row.childNodes[tgtColumnIndex];
            var rowCellTarget = getChildForDirection(tgtNode, upBool);
            moveCursorToStartOfElement(rowCellTarget || tgtNode);
          }
          /**
           * @param {?} preBrowserNode
           * @return {?}
           */
          function shouldFixCaret(preBrowserNode) {
            var parent = editor.selection.getNode();
            var newParent = editor.dom.getParent(parent, "td,th");
            var oldParent = editor.dom.getParent(preBrowserNode, "td,th");
            return newParent && newParent !== oldParent && checkSameParentTable(newParent, oldParent);
          }
          /**
           * @param {?} nodeOne
           * @param {?} NodeTwo
           * @return {?}
           */
          function checkSameParentTable(nodeOne, NodeTwo) {
            return editor.dom.getParent(nodeOne, "TABLE") === editor.dom.getParent(NodeTwo, "TABLE");
          }
          var key = event.keyCode;
          if (isVerticalMovement() && isInTable(editor)) {
            var preBrowserNode = editor.selection.getNode();
            setTimeout(function() {
              if (shouldFixCaret(preBrowserNode)) {
                handle(!event.shiftKey && key === keys.UP, preBrowserNode, event);
              }
            }, 0);
          }
        }
        editor.on("KeyDown", function(down) {
          moveSelection(down);
        });
      }
      /**
       * @return {undefined}
       */
      function fixBeforeTableCaretBug() {
        /**
         * @param {!Object} rng
         * @param {!Object} par
         * @return {?}
         */
        function isAtStart(rng, par) {
          var c;
          var doc = par.ownerDocument;
          var rng2 = doc.createRange();
          return rng2.setStartBefore(par), rng2.setEnd(rng.endContainer, rng.endOffset), c = doc.createElement("body"), c.appendChild(rng2.cloneContents()), 0 === c.innerHTML.replace(/<(br|img|object|embed|input|textarea)[^>]*>/gi, "-").replace(/<[^>]+>/g, "").length;
        }
        editor.on("KeyDown", function(event) {
          var rng;
          var table;
          var dom = editor.dom;
          if (37 == event.keyCode || 38 == event.keyCode) {
            rng = editor.selection.getRng();
            table = dom.getParent(rng.startContainer, "table");
            if (table && editor.getBody().firstChild == table && isAtStart(rng, table)) {
              rng = dom.createRng();
              rng.setStartBefore(table);
              rng.setEndBefore(table);
              editor.selection.setRng(rng);
              event.preventDefault();
            }
          }
        });
      }
      /**
       * @return {undefined}
       */
      function fixTableCaretPos() {
        editor.on("KeyDown SetContent VisualAid", function() {
          var node;
          node = editor.getBody().lastChild;
          for (; node; node = node.previousSibling) {
            if (3 == node.nodeType) {
              if (node.nodeValue.length > 0) {
                break;
              }
            } else {
              if (1 == node.nodeType && ("BR" == node.tagName || !node.getAttribute("data-mce-bogus"))) {
                break;
              }
            }
          }
          if (node && "TABLE" == node.nodeName) {
            if (editor.settings.forced_root_block) {
              editor.dom.add(editor.getBody(), editor.settings.forced_root_block, editor.settings.forced_root_block_attrs, Env.ie && Env.ie < 11 ? "&nbsp;" : '<br data-mce-bogus="1" />');
            } else {
              editor.dom.add(editor.getBody(), "br", {
                "data-mce-bogus" : "1"
              });
            }
          }
        });
        editor.on("PreProcess", function(rule) {
          var node = rule.node.lastChild;
          if (node && ("BR" == node.nodeName || 1 == node.childNodes.length && ("BR" == node.firstChild.nodeName || "\u00a0" == node.firstChild.nodeValue)) && node.previousSibling && "TABLE" == node.previousSibling.nodeName) {
            editor.dom.remove(node);
          }
        });
      }
      /**
       * @return {undefined}
       */
      function fixTableCellSelection() {
        /**
         * @param {!Object} ed
         * @param {!Range} rng
         * @param {!Object} n
         * @param {string} fn
         * @return {?}
         */
        function tableCellSelected(ed, rng, n, fn) {
          var e;
          var rslt;
          var winRef;
          /** @type {number} */
          var nodeType = 3;
          var c = ed.dom.getParent(rng.startContainer, "TABLE");
          return c && (e = c.parentNode), rslt = rng.startContainer.nodeType == nodeType && 0 === rng.startOffset && 0 === rng.endOffset && fn && ("TR" == n.nodeName || n == e), winRef = ("TD" == n.nodeName || "TH" == n.nodeName) && !fn, rslt || winRef;
        }
        /**
         * @return {undefined}
         */
        function fixSelection() {
          var rng = editor.selection.getRng();
          var n = editor.selection.getNode();
          var e = editor.dom.getParent(rng.startContainer, "TD,TH");
          if (tableCellSelected(editor, rng, n, e)) {
            if (!e) {
              e = n;
            }
            var node = e.lastChild;
            for (; node.lastChild;) {
              node = node.lastChild;
            }
            if (3 == node.nodeType) {
              rng.setEnd(node, node.data.length);
              editor.selection.setRng(rng);
            }
          }
        }
        editor.on("KeyDown", function() {
          fixSelection();
        });
        editor.on("MouseDown", function(event) {
          if (2 != event.button) {
            fixSelection();
          }
        });
      }
      /**
       * @return {undefined}
       */
      function deleteTable() {
        editor.on("keydown", function(e) {
          if ((e.keyCode == keys.DELETE || e.keyCode == keys.BACKSPACE) && !e.isDefaultPrevented()) {
            var table = editor.dom.getParent(editor.selection.getStart(), "table");
            if (table) {
              var cells = editor.dom.select("td,th", table);
              var i = cells.length;
              for (; i--;) {
                if (!editor.dom.hasClass(cells[i], "mce-item-selected")) {
                  return;
                }
              }
              e.preventDefault();
              editor.execCommand("mceTableDelete");
            }
          }
        });
      }
      deleteTable();
      if (Env.webkit) {
        handleContentEditableSelection();
        fixTableCellSelection();
      }
      if (Env.gecko) {
        fixBeforeTableCaretBug();
        fixTableCaretPos();
      }
      if (Env.ie > 10) {
        fixBeforeTableCaretBug();
        fixTableCaretPos();
      }
    };
  });
  define("tinymce/tableplugin/CellSelection", ["tinymce/tableplugin/TableGrid", "tinymce/dom/TreeWalker", "tinymce/util/Tools"], function(TableGrid, TreeWalker, a) {
    return function(editor) {
      /**
       * @param {boolean} type
       * @return {undefined}
       */
      function clear(type) {
        /** @type {string} */
        editor.getBody().style.webkitUserSelect = "";
        if (type || candlesticks) {
          editor.dom.removeClass(editor.dom.select("td.mce-item-selected,th.mce-item-selected"), "mce-item-selected");
          /** @type {boolean} */
          candlesticks = false;
        }
      }
      /**
       * @param {!Event} e
       * @return {undefined}
       */
      function cellSelectionHandler(e) {
        var sel;
        var table;
        var target = e.target;
        if (!j && startCell && (tableGrid || target != startCell) && ("TD" == target.nodeName || "TH" == target.nodeName)) {
          table = dom.getParent(target, "table");
          if (table == startTable) {
            if (!tableGrid) {
              tableGrid = new TableGrid(editor, table);
              tableGrid.setStartCell(startCell);
              /** @type {string} */
              editor.getBody().style.webkitUserSelect = "none";
            }
            tableGrid.setEndCell(target);
            /** @type {boolean} */
            candlesticks = true;
          }
          sel = editor.selection.getSel();
          try {
            if (sel.removeAllRanges) {
              sel.removeAllRanges();
            } else {
              sel.empty();
            }
          } catch (m) {
          }
          e.preventDefault();
        }
      }
      var tableGrid;
      var startCell;
      var startTable;
      var j;
      var dom = editor.dom;
      /** @type {boolean} */
      var candlesticks = true;
      return editor.on("MouseDown", function(event) {
        if (!(2 == event.button || j)) {
          clear();
          startCell = dom.getParent(event.target, "td,th");
          startTable = dom.getParent(startCell, "table");
        }
      }), editor.on("mouseover", cellSelectionHandler), editor.on("remove", function() {
        dom.unbind(editor.getDoc(), "mouseover", cellSelectionHandler);
      }), editor.on("MouseUp", function() {
        /**
         * @param {!Object} node
         * @param {number} left
         * @return {?}
         */
        function setPoint(node, left) {
          var walker = new TreeWalker(node, node);
          do {
            if (3 == node.nodeType && 0 !== a.trim(node.nodeValue).length) {
              return void(left ? rng.setStart(node, 0) : rng.setEnd(node, node.nodeValue.length));
            }
            if ("BR" == node.nodeName) {
              return void(left ? rng.setStartBefore(node) : rng.setEndBefore(node));
            }
          } while (node = left ? walker.next() : walker.prev());
        }
        var rng;
        var nodes;
        var walker;
        var node;
        var lastNode;
        var selection = editor.selection;
        if (startCell) {
          if (tableGrid && (editor.getBody().style.webkitUserSelect = ""), nodes = dom.select("td.mce-item-selected,th.mce-item-selected"), nodes.length > 0) {
            rng = dom.createRng();
            node = nodes[0];
            rng.setStartBefore(node);
            rng.setEndAfter(node);
            setPoint(node, 1);
            walker = new TreeWalker(node, dom.getParent(nodes[0], "table"));
            do {
              if ("TD" == node.nodeName || "TH" == node.nodeName) {
                if (!dom.hasClass(node, "mce-item-selected")) {
                  break;
                }
                lastNode = node;
              }
            } while (node = walker.next());
            setPoint(lastNode);
            selection.setRng(rng);
          }
          editor.nodeChanged();
          /** @type {null} */
          startCell = tableGrid = startTable = null;
        }
      }), editor.on("KeyUp Drop SetContent", function(verifiedEvent) {
        clear("setcontent" == verifiedEvent.type);
        /** @type {null} */
        startCell = tableGrid = startTable = null;
        /** @type {boolean} */
        j = false;
      }), editor.on("ObjectResizeStart ObjectResized", function(verifiedEvent) {
        /** @type {boolean} */
        j = "objectresized" != verifiedEvent.type;
      }), {
        clear : clear
      };
    };
  });
  define("tinymce/tableplugin/Dialogs", ["tinymce/util/Tools", "tinymce/Env"], function(Tools, Env) {
    var each = Tools.each;
    return function(editor) {
      /**
       * @return {?}
       */
      function createColorPickAction() {
        var colorPickerCallback = editor.settings.color_picker_callback;
        return colorPickerCallback ? function() {
          var elementValues = this;
          colorPickerCallback.call(editor, function(val) {
            elementValues.value(val).fire("change");
          }, elementValues.value());
        } : void 0;
      }
      /**
       * @param {?} dom
       * @return {?}
       */
      function createStyleForm(dom) {
        return {
          title : "Advanced",
          type : "form",
          defaults : {
            onchange : function() {
              updateStyle(dom, this.parents().reverse()[0], "style" == this.name());
            }
          },
          items : [{
            label : "Style",
            name : "style",
            type : "textbox"
          }, {
            type : "form",
            padding : 0,
            formItemDefaults : {
              layout : "grid",
              alignH : ["start", "right"]
            },
            defaults : {
              size : 7
            },
            items : [{
              label : "Border color",
              type : "colorbox",
              name : "borderColor",
              onaction : createColorPickAction()
            }, {
              label : "Background color",
              type : "colorbox",
              name : "backgroundColor",
              onaction : createColorPickAction()
            }]
          }]
        };
      }
      /**
       * @param {string} size
       * @return {?}
       */
      function removePxSuffix(size) {
        return size ? size.replace(/px$/, "") : "";
      }
      /**
       * @param {string} size
       * @return {?}
       */
      function addSizeSuffix(size) {
        return /^[0-9]+$/.test(size) && (size = size + "px"), size;
      }
      /**
       * @param {!Node} elm
       * @return {undefined}
       */
      function unApplyAlign(elm) {
        each("left center right".split(" "), function(name) {
          editor.formatter.remove("align" + name, {}, elm);
        });
      }
      /**
       * @param {!Node} elm
       * @return {undefined}
       */
      function unApplyVAlign(elm) {
        each("top middle bottom".split(" "), function(name) {
          editor.formatter.remove("valign" + name, {}, elm);
        });
      }
      /**
       * @param {?} inputList
       * @param {!Function} itemCallback
       * @param {number} startItems
       * @return {?}
       */
      function buildListItems(inputList, itemCallback, startItems) {
        /**
         * @param {?} values
         * @param {number} items
         * @return {?}
         */
        function appendItems(values, items) {
          return items = items || [], Tools.each(values, function(item) {
            var menuItem = {
              text : item.text || item.title
            };
            if (item.menu) {
              menuItem.menu = appendItems(item.menu);
            } else {
              menuItem.value = item.value;
              if (itemCallback) {
                itemCallback(menuItem);
              }
            }
            items.push(menuItem);
          }), items;
        }
        return appendItems(inputList, startItems || []);
      }
      /**
       * @param {?} dom
       * @param {!Object} win
       * @param {boolean} source
       * @return {undefined}
       */
      function updateStyle(dom, win, source) {
        var data = win.toJSON();
        var css = dom.parseStyle(data.style);
        if (source) {
          win.find("#borderColor").value(css["border-color"] || "")[0].fire("change");
          win.find("#backgroundColor").value(css["background-color"] || "")[0].fire("change");
        } else {
          css["border-color"] = data.borderColor;
          css["background-color"] = data.backgroundColor;
        }
        win.find("#style").value(dom.serializeStyle(dom.parseStyle(dom.serializeStyle(css))));
      }
      /**
       * @param {?} dom
       * @param {!Element} data
       * @param {!Object} elm
       * @return {undefined}
       */
      function appendStylesToData(dom, data, elm) {
        var css = dom.parseStyle(dom.getAttrib(elm, "style"));
        if (css["border-color"]) {
          data.borderColor = css["border-color"];
        }
        if (css["background-color"]) {
          data.backgroundColor = css["background-color"];
        }
        data.style = dom.serializeStyle(css);
      }
      var dialogs = this;
      /**
       * @return {undefined}
       */
      dialogs.tableProps = function() {
        dialogs.table(true);
      };
      /**
       * @param {boolean} table
       * @return {undefined}
       */
      dialogs.table = function(table) {
        /**
         * @return {undefined}
         */
        function onSubmitTableForm() {
          var captionElm;
          updateStyle(dom, this);
          data = Tools.extend(data, this.toJSON());
          Tools.each("backgroundColor borderColor".split(" "), function(unbracketed) {
            delete data[unbracketed];
          });
          if (data["class"] === false) {
            delete data["class"];
          }
          editor.undoManager.transact(function() {
            if (!tableElm) {
              tableElm = editor.plugins.table.insertTable(data.cols || 1, data.rows || 1);
            }
            if (data.d3 !== "") {
              /** @type {string} */
              var rows = tableElm.getElementsByTagName('tr');
              var elementsOfFirstRow = rows[0].getElementsByTagName('td');
              Array.from(elementsOfFirstRow).forEach(function(element){
	              var newElement = document.createElement('th');
	              newElement.innerHTML = element.innerHTML;
                element.parentNode.replaceChild(newElement, element);
              });
              if(!data["class"].includes("d3")){
                data["class"] = data["class"] + ' d3';
              }

            } else {
              if(data["class"].includes("d3")){
                data["class"] = data["class"].replace(' d3', '');
              }
            }
            editor.dom.setAttribs(tableElm, {
              cellspacing : data.cellspacing,
              cellpadding : data.cellpadding,
              border : data.border,
              style : data.style,
              "class" : data["class"]
            });
            if (dom.getAttrib(tableElm, "width")) {
              dom.setAttrib(tableElm, "width", removePxSuffix(data.width));
            } else {
              dom.setStyle(tableElm, "width", addSizeSuffix(data.width));
            }
            if (data.d3) {
              dom.setAttrib(tableElm, "data-d3-chart", data.d3);
            } else {
              dom.setAttrib(tableElm, "data-d3-chart", null);
            }
            dom.setStyle(tableElm, "height", addSizeSuffix(data.height));
            captionElm = dom.select("caption", tableElm)[0];
            if (captionElm && !data.caption) {
              dom.remove(captionElm);
            }
            if (!captionElm && data.caption) {
              captionElm = dom.create("caption");
              /** @type {string} */
              captionElm.innerHTML = Env.ie ? "\u00a0" : '<br data-mce-bogus="1"/>';
              tableElm.insertBefore(captionElm, tableElm.firstChild);
            }
            unApplyAlign(tableElm);
            if (data.align) {
              editor.formatter.apply("align" + data.align, {}, tableElm);
            }
            editor.focus();
            editor.addVisual();
          });
        }
        var tableElm;
        var colsCtrl;
        var rowsCtrl;
        var classListCtrl;
        var generalTableForm;
        var dom = editor.dom;
        var data = {};
        if (table === true) {
          tableElm = dom.getParent(editor.selection.getStart(), "table");
          if (tableElm) {
            data = {
              width : removePxSuffix(dom.getStyle(tableElm, "width") || dom.getAttrib(tableElm, "width")),
              height : removePxSuffix(dom.getStyle(tableElm, "height") || dom.getAttrib(tableElm, "height")),
              cellspacing : tableElm ? dom.getAttrib(tableElm, "cellspacing") : "",
              cellpadding : tableElm ? dom.getAttrib(tableElm, "cellpadding") : "",
              border : tableElm ? dom.getAttrib(tableElm, "border") : "",
              caption : !!dom.select("caption", tableElm)[0],
              "class" : dom.getAttrib(tableElm, "class"),
              d3 : dom.getAttrib(tableElm, "data-d3-chart")
            };
            each("left center right".split(" "), function(name) {
              if (editor.formatter.matchNode(tableElm, "align" + name)) {
                /** @type {string} */
                data.align = name;
              }
            });
          }
        } else {
          colsCtrl = {
            label : "Cols",
            name : "cols"
          };
          rowsCtrl = {
            label : "Rows",
            name : "rows"
          };
        }
        if (editor.settings.table_class_list) {
          if (data["class"]) {
            data["class"] = data["class"].replace(/\s*mce\-item\-table\s*/g, "");
          }
          classListCtrl = {
            name : "class",
            type : "listbox",
            label : "Class",
            values : buildListItems(editor.settings.table_class_list, function(s) {
              if (s.value) {
                /**
                 * @return {?}
                 */
                s.textStyle = function() {
                  return editor.formatter.getCssText({
                    block : "table",
                    classes : [s.value]
                  });
                };
              }
            })
          };
        }
        generalTableForm = {
          type : "form",
          layout : "flex",
          direction : "column",
          labelGapCalc : "children",
          padding : 0,
          items : [{
            type : "form",
            labelGapCalc : false,
            padding : 0,
            layout : "grid",
            columns : 2,
            defaults : {
              type : "textbox",
              maxWidth : 50
            },
            items : [colsCtrl, rowsCtrl, {
              label : "Width",
              name : "width"
            }, {
              label : "Height",
              name : "height"
            }, {
              label : "Cell spacing",
              name : "cellspacing"
            }, {
              label : "Cell padding",
              name : "cellpadding"
            }, {
              label : "Border",
              name : "border"
            }, {
              label : "Caption",
              name : "caption",
              type : "checkbox"
            }]
          }, {
            label : "Alignment",
            name : "align",
            type : "listbox",
            text : "None",
            values : [{
              text : "None",
              value : ""
            }, {
              text : "Left",
              value : "left"
            }, {
              text : "Center",
              value : "center"
            }, {
              text : "Right",
              value : "right"
            }]
          },{
            type: "label",
            text: "Note: When using a D3 table type, be sure that the first row of your table contains the table headers."
          }, {
            label : "D3 Table",
            name : "d3",
            type : "listbox",
            text : "None",
            values : [{
              text : "None",
              value : ""
            }, {
              text : "Donut",
              value : "donut"
            }, {
              text : "Bar",
              value : "bar"
            }, {
              text : "Pie",
              value : "pie"
            }]
          }, classListCtrl]
        };
        if (editor.settings.table_advtab !== false) {
          appendStylesToData(dom, data, tableElm);
          editor.windowManager.open({
            title : "Table properties",
            data : data,
            bodyType : "tabpanel",
            body : [{
              title : "General",
              type : "form",
              items : generalTableForm
            }, createStyleForm(dom)],
            onsubmit : onSubmitTableForm
          });
        } else {
          editor.windowManager.open({
            title : "Table properties",
            data : data,
            body : generalTableForm,
            onsubmit : onSubmitTableForm
          });
        }
      };
      /**
       * @param {!Object} grid
       * @param {string} cell
       * @return {undefined}
       */
      dialogs.merge = function(grid, cell) {
        editor.windowManager.open({
          title : "Merge cells",
          body : [{
            label : "Cols",
            name : "cols",
            type : "textbox",
            value : "1",
            size : 10
          }, {
            label : "Rows",
            name : "rows",
            type : "textbox",
            value : "1",
            size : 10
          }],
          onsubmit : function() {
            var data = this.toJSON();
            editor.undoManager.transact(function() {
              grid.merge(cell, data.cols, data.rows);
            });
          }
        });
      };
      /**
       * @return {undefined}
       */
      dialogs.cell = function() {
        /**
         * @return {undefined}
         */
        function onSubmitCellForm() {
          updateStyle(dom, this);
          data = Tools.extend(data, this.toJSON());
          editor.undoManager.transact(function() {
            each(cells, function(cellElm) {
              editor.dom.setAttribs(cellElm, {
                scope : data.scope,
                style : data.style,
                "class" : data["class"]
              });
              editor.dom.setStyles(cellElm, {
                width : addSizeSuffix(data.width),
                height : addSizeSuffix(data.height)
              });
              if (data.type && cellElm.nodeName.toLowerCase() != data.type) {
                cellElm = dom.rename(cellElm, data.type);
              }
              unApplyAlign(cellElm);
              if (data.align) {
                editor.formatter.apply("align" + data.align, {}, cellElm);
              }
              unApplyVAlign(cellElm);
              if (data.valign) {
                editor.formatter.apply("valign" + data.valign, {}, cellElm);
              }
            });
            editor.focus();
          });
        }
        var cellElm;
        var data;
        var classListCtrl;
        var dom = editor.dom;
        /** @type {!Array} */
        var cells = [];
        if (cells = editor.dom.select("td.mce-item-selected,th.mce-item-selected"), cellElm = editor.dom.getParent(editor.selection.getStart(), "td,th"), !cells.length && cellElm && cells.push(cellElm), cellElm = cellElm || cells[0]) {
          data = {
            width : removePxSuffix(dom.getStyle(cellElm, "width") || dom.getAttrib(cellElm, "width")),
            height : removePxSuffix(dom.getStyle(cellElm, "height") || dom.getAttrib(cellElm, "height")),
            scope : dom.getAttrib(cellElm, "scope"),
            "class" : dom.getAttrib(cellElm, "class")
          };
          data.type = cellElm.nodeName.toLowerCase();
          each("left center right".split(" "), function(name) {
            if (editor.formatter.matchNode(cellElm, "align" + name)) {
              /** @type {string} */
              data.align = name;
            }
          });
          each("top middle bottom".split(" "), function(name) {
            if (editor.formatter.matchNode(cellElm, "valign" + name)) {
              /** @type {string} */
              data.valign = name;
            }
          });
          if (editor.settings.table_cell_class_list) {
            classListCtrl = {
              name : "class",
              type : "listbox",
              label : "Class",
              values : buildListItems(editor.settings.table_cell_class_list, function(s) {
                if (s.value) {
                  /**
                   * @return {?}
                   */
                  s.textStyle = function() {
                    return editor.formatter.getCssText({
                      block : "td",
                      classes : [s.value]
                    });
                  };
                }
              })
            };
          }
          var generalCellForm = {
            type : "form",
            layout : "flex",
            direction : "column",
            labelGapCalc : "children",
            padding : 0,
            items : [{
              type : "form",
              layout : "grid",
              columns : 2,
              labelGapCalc : false,
              padding : 0,
              defaults : {
                type : "textbox",
                maxWidth : 50
              },
              items : [{
                label : "Width",
                name : "width"
              }, {
                label : "Height",
                name : "height"
              }, {
                label : "Cell type",
                name : "type",
                type : "listbox",
                text : "None",
                minWidth : 90,
                maxWidth : null,
                values : [{
                  text : "Cell",
                  value : "td"
                }, {
                  text : "Header cell",
                  value : "th"
                }]
              }, {
                label : "Scope",
                name : "scope",
                type : "listbox",
                text : "None",
                minWidth : 90,
                maxWidth : null,
                values : [{
                  text : "None",
                  value : ""
                }, {
                  text : "Row",
                  value : "row"
                }, {
                  text : "Column",
                  value : "col"
                }, {
                  text : "Row group",
                  value : "rowgroup"
                }, {
                  text : "Column group",
                  value : "colgroup"
                }]
              }, {
                label : "H Align",
                name : "align",
                type : "listbox",
                text : "None",
                minWidth : 90,
                maxWidth : null,
                values : [{
                  text : "None",
                  value : ""
                }, {
                  text : "Left",
                  value : "left"
                }, {
                  text : "Center",
                  value : "center"
                }, {
                  text : "Right",
                  value : "right"
                }]
              }, {
                label : "V Align",
                name : "valign",
                type : "listbox",
                text : "None",
                minWidth : 90,
                maxWidth : null,
                values : [{
                  text : "None",
                  value : ""
                }, {
                  text : "Top",
                  value : "top"
                }, {
                  text : "Middle",
                  value : "middle"
                }, {
                  text : "Bottom",
                  value : "bottom"
                }]
              }]
            }, classListCtrl]
          };
          if (editor.settings.table_cell_advtab !== false) {
            appendStylesToData(dom, data, cellElm);
            editor.windowManager.open({
              title : "Cell properties",
              bodyType : "tabpanel",
              data : data,
              body : [{
                title : "General",
                type : "form",
                items : generalCellForm
              }, createStyleForm(dom)],
              onsubmit : onSubmitCellForm
            });
          } else {
            editor.windowManager.open({
              title : "Cell properties",
              data : data,
              body : generalCellForm,
              onsubmit : onSubmitCellForm
            });
          }
        }
      };
      /**
       * @return {undefined}
       */
      dialogs.row = function() {
        /**
         * @return {undefined}
         */
        function onSubmitRowForm() {
          var tableElm;
          var oldParentElm;
          var parentElm;
          updateStyle(dom, this);
          data = Tools.extend(data, this.toJSON());
          editor.undoManager.transact(function() {
            var toType = data.type;
            each(rows, function(rowElm) {
              editor.dom.setAttribs(rowElm, {
                scope : data.scope,
                style : data.style,
                "class" : data["class"]
              });
              editor.dom.setStyles(rowElm, {
                height : addSizeSuffix(data.height)
              });
              if (toType != rowElm.parentNode.nodeName.toLowerCase()) {
                tableElm = dom.getParent(rowElm, "table");
                oldParentElm = rowElm.parentNode;
                parentElm = dom.select(toType, tableElm)[0];
                if (!parentElm) {
                  parentElm = dom.create(toType);
                  if (tableElm.firstChild) {
                    tableElm.insertBefore(parentElm, tableElm.firstChild);
                  } else {
                    tableElm.appendChild(parentElm);
                  }
                }
                parentElm.appendChild(rowElm);
                if (!oldParentElm.hasChildNodes()) {
                  dom.remove(oldParentElm);
                }
              }
              unApplyAlign(rowElm);
              if (data.align) {
                editor.formatter.apply("align" + data.align, {}, rowElm);
              }
            });
            editor.focus();
          });
        }
        var tableElm;
        var cellElm;
        var rowElm;
        var classListCtrl;
        var data;
        var generalRowForm;
        var dom = editor.dom;
        /** @type {!Array} */
        var rows = [];
        tableElm = editor.dom.getParent(editor.selection.getStart(), "table");
        cellElm = editor.dom.getParent(editor.selection.getStart(), "td,th");
        each(tableElm.rows, function(row) {
          each(row.cells, function(cell) {
            return dom.hasClass(cell, "mce-item-selected") || cell == cellElm ? (rows.push(row), false) : void 0;
          });
        });
        rowElm = rows[0];
        if (rowElm) {
          data = {
            height : removePxSuffix(dom.getStyle(rowElm, "height") || dom.getAttrib(rowElm, "height")),
            scope : dom.getAttrib(rowElm, "scope"),
            "class" : dom.getAttrib(rowElm, "class")
          };
          data.type = rowElm.parentNode.nodeName.toLowerCase();
          each("left center right".split(" "), function(name) {
            if (editor.formatter.matchNode(rowElm, "align" + name)) {
              /** @type {string} */
              data.align = name;
            }
          });
          if (editor.settings.table_row_class_list) {
            classListCtrl = {
              name : "class",
              type : "listbox",
              label : "Class",
              values : buildListItems(editor.settings.table_row_class_list, function(s) {
                if (s.value) {
                  /**
                   * @return {?}
                   */
                  s.textStyle = function() {
                    return editor.formatter.getCssText({
                      block : "tr",
                      classes : [s.value]
                    });
                  };
                }
              })
            };
          }
          generalRowForm = {
            type : "form",
            columns : 2,
            padding : 0,
            defaults : {
              type : "textbox"
            },
            items : [{
              type : "listbox",
              name : "type",
              label : "Row type",
              text : "None",
              maxWidth : null,
              values : [{
                text : "Header",
                value : "thead"
              }, {
                text : "Body",
                value : "tbody"
              }, {
                text : "Footer",
                value : "tfoot"
              }]
            }, {
              type : "listbox",
              name : "align",
              label : "Alignment",
              text : "None",
              maxWidth : null,
              values : [{
                text : "None",
                value : ""
              }, {
                text : "Left",
                value : "left"
              }, {
                text : "Center",
                value : "center"
              }, {
                text : "Right",
                value : "right"
              }]
            }, {
              label : "Height",
              name : "height"
            }, classListCtrl]
          };
          if (editor.settings.table_row_advtab !== false) {
            appendStylesToData(dom, data, rowElm);
            editor.windowManager.open({
              title : "Row properties",
              data : data,
              bodyType : "tabpanel",
              body : [{
                title : "General",
                type : "form",
                items : generalRowForm
              }, createStyleForm(dom)],
              onsubmit : onSubmitRowForm
            });
          } else {
            editor.windowManager.open({
              title : "Row properties",
              data : data,
              body : generalRowForm,
              onsubmit : onSubmitRowForm
            });
          }
        }
      };
    };
  });
  define("tinymce/tableplugin/Plugin", ["tinymce/tableplugin/TableGrid", "tinymce/tableplugin/Quirks", "tinymce/tableplugin/CellSelection", "tinymce/tableplugin/Dialogs", "tinymce/util/Tools", "tinymce/dom/TreeWalker", "tinymce/Env", "tinymce/PluginManager"], function(TableGrid, Quirks, CellSelection, Dialogs, Tools, canCreateDiscussions, Env, PluginManager) {
    /**
     * @param {!Object} editor
     * @return {undefined}
     */
    function Plugin(editor) {
      /**
       * @param {string} command
       * @return {?}
       */
      function cmd(command) {
        return function() {
          editor.execCommand(command);
        };
      }
      /**
       * @param {(number|string)} object
       * @param {(number|string)} callback
       * @return {?}
       */
      function insertTable(object, callback) {
        var _num2;
        var topIndex;
        var html;
        var tableElm;
        /** @type {string} */
        html = '<table id="__mce"><tbody>';
        /** @type {number} */
        _num2 = 0;
        for (; callback > _num2; _num2++) {
          /** @type {string} */
          html = html + "<tr>";
          /** @type {number} */
          topIndex = 0;
          for (; object > topIndex; topIndex++) {
            /** @type {string} */
            html = html + ("<td>" + (Env.ie ? " " : "<br>") + "</td>");
          }
          /** @type {string} */
          html = html + "</tr>";
        }
        return html = html + "</tbody></table>", editor.undoManager.transact(function() {
          editor.insertContent(html);
          tableElm = editor.dom.get("__mce");
          editor.dom.setAttrib(tableElm, "id", null);
          editor.dom.setAttribs(tableElm, editor.settings.table_default_attributes || {});
          editor.dom.setStyles(tableElm, editor.settings.table_default_styles || {});
        }), tableElm;
      }
      /**
       * @param {!Element} ctrl
       * @param {string} selector
       * @return {undefined}
       */
      function handleDisabledState(ctrl, selector) {
        /**
         * @return {undefined}
         */
        function bindStateListener() {
          ctrl.disabled(!editor.dom.getParent(editor.selection.getStart(), selector));
          editor.selection.selectorChanged(selector, function(state) {
            ctrl.disabled(!state);
          });
        }
        if (editor.initialized) {
          bindStateListener();
        } else {
          editor.on("init", bindStateListener);
        }
      }
      /**
       * @return {undefined}
       */
      function postRender() {
        handleDisabledState(this, "table");
      }
      /**
       * @return {undefined}
       */
      function postRenderCell() {
        handleDisabledState(this, "td,th");
      }
      /**
       * @return {?}
       */
      function generateTableGrid() {
        /** @type {string} */
        var pix_color = "";
        /** @type {string} */
        pix_color = '<table role="grid" class="mce-grid mce-grid-border" aria-readonly="true">';
        /** @type {number} */
        var d = 0;
        for (; 10 > d; d++) {
          /** @type {string} */
          pix_color = pix_color + "<tr>";
          /** @type {number} */
          var c = 0;
          for (; 10 > c; c++) {
            /** @type {string} */
            pix_color = pix_color + ('<td role="gridcell" tabindex="-1"><a id="mcegrid' + (10 * d + c) + '" href="#" data-mce-x="' + c + '" data-mce-y="' + d + '"></a></td>');
          }
          /** @type {string} */
          pix_color = pix_color + "</tr>";
        }
        return pix_color = pix_color + "</table>", pix_color = pix_color + '<div class="mce-text-center" role="presentation">1 x 1</div>';
      }
      /**
       * @param {number} tx
       * @param {number} ty
       * @param {!Object} control
       * @return {?}
       */
      function selectGrid(tx, ty, control) {
        var x;
        var y;
        var gmail_inputelem;
        var elem;
        var isChatOpen;
        var group = control.getEl().getElementsByTagName("table")[0];
        var rtl = control.isRtl() || "tl-tr" == control.parent().rel;
        /** @type {string} */
        group.nextSibling.innerHTML = tx + 1 + " x " + (ty + 1);
        if (rtl) {
          /** @type {number} */
          tx = 9 - tx;
        }
        /** @type {number} */
        y = 0;
        for (; 10 > y; y++) {
          /** @type {number} */
          x = 0;
          for (; 10 > x; x++) {
            elem = group.rows[y].childNodes[x].firstChild;
            /** @type {boolean} */
            isChatOpen = (rtl ? x >= tx : tx >= x) && ty >= y;
            editor.dom.toggleClass(elem, "mce-active", isChatOpen);
            if (isChatOpen) {
              gmail_inputelem = elem;
            }
          }
        }
        return gmail_inputelem.parentNode;
      }
      var clipboardRows;
      var self = this;
      var dialogs = new Dialogs(editor);
      if (editor.settings.table_grid === false) {
        editor.addMenuItem("inserttable", {
          text : "Insert table",
          icon : "table",
          context : "table",
          onclick : dialogs.table
        });
      } else {
        editor.addMenuItem("inserttable", {
          text : "Insert table",
          icon : "table",
          context : "table",
          ariaHideMenu : true,
          onclick : function(e) {
            if (e.aria) {
              this.parent().hideAll();
              e.stopImmediatePropagation();
              dialogs.table();
            }
          },
          onshow : function() {
            selectGrid(0, 0, this.menu.items()[0]);
          },
          onhide : function() {
            var className = this.menu.items()[0].getEl().getElementsByTagName("a");
            editor.dom.removeClass(className, "mce-active");
            editor.dom.addClass(className[0], "mce-active");
          },
          menu : [{
            type : "container",
            html : generateTableGrid(),
            onPostRender : function() {
              /** @type {number} */
              this.lastX = this.lastY = 0;
            },
            onmousemove : function(e) {
              var x;
              var y;
              var target = e.target;
              if ("A" == target.tagName.toUpperCase()) {
                /** @type {number} */
                x = parseInt(target.getAttribute("data-mce-x"), 10);
                /** @type {number} */
                y = parseInt(target.getAttribute("data-mce-y"), 10);
                if (this.isRtl() || "tl-tr" == this.parent().rel) {
                  /** @type {number} */
                  x = 9 - x;
                }
                if (x !== this.lastX || y !== this.lastY) {
                  selectGrid(x, y, e.control);
                  this.lastX = x;
                  /** @type {number} */
                  this.lastY = y;
                }
              }
            },
            onclick : function(e) {
              var self = this;
              if ("A" == e.target.tagName.toUpperCase()) {
                e.preventDefault();
                e.stopPropagation();
                self.parent().cancel();
                editor.undoManager.transact(function() {
                  insertTable(self.lastX + 1, self.lastY + 1);
                });
                editor.addVisual();
              }
            }
          }]
        });
      }
      editor.addMenuItem("tableprops", {
        text : "Table properties",
        context : "table",
        onPostRender : postRender,
        onclick : dialogs.tableProps
      });
      editor.addMenuItem("deletetable", {
        text : "Delete table",
        context : "table",
        onPostRender : postRender,
        cmd : "mceTableDelete"
      });
      editor.addMenuItem("cell", {
        separator : "before",
        text : "Cell",
        context : "table",
        menu : [{
          text : "Cell properties",
          onclick : cmd("mceTableCellProps"),
          onPostRender : postRenderCell
        }, {
          text : "Merge cells",
          onclick : cmd("mceTableMergeCells"),
          onPostRender : postRenderCell
        }, {
          text : "Split cell",
          onclick : cmd("mceTableSplitCells"),
          onPostRender : postRenderCell
        }]
      });
      editor.addMenuItem("row", {
        text : "Row",
        context : "table",
        menu : [{
          text : "Insert row before",
          onclick : cmd("mceTableInsertRowBefore"),
          onPostRender : postRenderCell
        }, {
          text : "Insert row after",
          onclick : cmd("mceTableInsertRowAfter"),
          onPostRender : postRenderCell
        }, {
          text : "Delete row",
          onclick : cmd("mceTableDeleteRow"),
          onPostRender : postRenderCell
        }, {
          text : "Row properties",
          onclick : cmd("mceTableRowProps"),
          onPostRender : postRenderCell
        }, {
          text : "-"
        }, {
          text : "Cut row",
          onclick : cmd("mceTableCutRow"),
          onPostRender : postRenderCell
        }, {
          text : "Copy row",
          onclick : cmd("mceTableCopyRow"),
          onPostRender : postRenderCell
        }, {
          text : "Paste row before",
          onclick : cmd("mceTablePasteRowBefore"),
          onPostRender : postRenderCell
        }, {
          text : "Paste row after",
          onclick : cmd("mceTablePasteRowAfter"),
          onPostRender : postRenderCell
        }]
      });
      editor.addMenuItem("column", {
        text : "Column",
        context : "table",
        menu : [{
          text : "Insert column before",
          onclick : cmd("mceTableInsertColBefore"),
          onPostRender : postRenderCell
        }, {
          text : "Insert column after",
          onclick : cmd("mceTableInsertColAfter"),
          onPostRender : postRenderCell
        }, {
          text : "Delete column",
          onclick : cmd("mceTableDeleteCol"),
          onPostRender : postRenderCell
        }]
      });
      /** @type {!Array} */
      var server_actions_menu = [];
      each("inserttable tableprops deletetable | cell row column".split(" "), function(name) {
        server_actions_menu.push("|" == name ? {
          text : "-"
        } : editor.menuItems[name]);
      });
      editor.addButton("table", {
        type : "menubutton",
        title : "Table",
        menu : server_actions_menu
      });
      if (!Env.isIE) {
        editor.on("click", function(e) {
          e = e.target;
          if ("TABLE" === e.nodeName) {
            editor.selection.select(e);
            editor.nodeChanged();
          }
        });
      }
      self.quirks = new Quirks(editor);
      editor.on("Init", function() {
        self.cellSelection = new CellSelection(editor);
      });
      each({
        mceTableSplitCells : function(grid) {
          grid.split();
        },
        mceTableMergeCells : function(grid) {
          var cell;
          cell = editor.dom.getParent(editor.selection.getStart(), "th,td");
          if (editor.dom.select("td.mce-item-selected,th.mce-item-selected").length) {
            grid.merge();
          } else {
            dialogs.merge(grid, cell);
          }
        },
        mceTableInsertRowBefore : function(grid) {
          grid.insertRow(true);
        },
        mceTableInsertRowAfter : function(grid) {
          grid.insertRow();
        },
        mceTableInsertColBefore : function(grid) {
          grid.insertCol(true);
        },
        mceTableInsertColAfter : function(grid) {
          grid.insertCol();
        },
        mceTableDeleteCol : function(grid) {
          grid.deleteCols();
        },
        mceTableDeleteRow : function(grid) {
          grid.deleteRows();
        },
        mceTableCutRow : function(grid) {
          clipboardRows = grid.cutRows();
        },
        mceTableCopyRow : function(grid) {
          clipboardRows = grid.copyRows();
        },
        mceTablePasteRowBefore : function(grid) {
          grid.pasteRows(clipboardRows, true);
        },
        mceTablePasteRowAfter : function(grid) {
          grid.pasteRows(clipboardRows);
        },
        mceTableDelete : function(grid) {
          grid.deleteTable();
        }
      }, function(modelCreatorFn, isAfterUserInput) {
        editor.addCommand(isAfterUserInput, function() {
          var grid = new TableGrid(editor);
          if (grid) {
            modelCreatorFn(grid);
            editor.execCommand("mceRepaint");
            self.cellSelection.clear();
          }
        });
      });
      each({
        mceInsertTable : dialogs.table,
        mceTableProps : function() {
          dialogs.table(true);
        },
        mceTableRowProps : dialogs.row,
        mceTableCellProps : dialogs.cell
      }, function(saveNotifs, isAfterUserInput) {
        editor.addCommand(isAfterUserInput, function(b, notifications) {
          saveNotifs(notifications);
        });
      });
      if (editor.settings.table_tab_navigation !== false) {
        editor.on("keydown", function(event) {
          var cellElm;
          var grid;
          var delta;
          if (9 == event.keyCode) {
            cellElm = editor.dom.getParent(editor.selection.getStart(), "th,td");
            if (cellElm) {
              event.preventDefault();
              grid = new TableGrid(editor);
              /** @type {number} */
              delta = event.shiftKey ? -1 : 1;
              editor.undoManager.transact(function() {
                if (!grid.moveRelIdx(cellElm, delta) && delta > 0) {
                  grid.insertRow();
                  grid.refresh();
                  grid.moveRelIdx(cellElm, delta);
                }
              });
            }
          }
        });
      }
      /** @type {function((number|string), (number|string)): ?} */
      self.insertTable = insertTable;
    }
    var each = Tools.each;
    PluginManager.add("plugin_edu.msu.anr.tinymce_table2", Plugin);
  });
}(this);
