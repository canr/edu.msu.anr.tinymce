tinymce.PluginManager.add('plugin_edu.msu.anr.tinymce_contentletcallout', function (editor) {

    const ATTR_NAME = 'data-identifier';
    var windowID;
    var folderTable= {};
    var contentTypes = {};
    var protocol = location.protocol;
    var slashes = protocol.concat("//");
    var host = slashes.concat(window.location.hostname);
    var busy = false;

    editor.onPostRender.add(function () {
        this.dom.addStyle(`
        .canr-callout {
            display: inline-block;
            width: 100%;
            height: 100px !important;
            margin: 10px 0;
            border: 1px solid black;
            background: lightgrey;
            color: transparent;
        }
		`);
    });

    function openInsertCalloutWindow() {
        let selectedNode = editor.selection.getNode();
        let contentletId = '';

        // Get current ID if cursor's on a callout
        if (selectedNode.tagName == 'DIV' && selectedNode.getAttribute(ATTR_NAME) !== null && selectedNode.getAttribute(ATTR_NAME) !== "") {
            contentletId = selectedNode.getAttribute(ATTR_NAME);
            if (editor.getDoc().getElementById('canr-callout-script')) {
                var canrScript = editor.getDoc().getElementById('canr-callout-script');
                canrScript.remove();
            }
            tinymce.activeEditor.dom.remove(selectedNode);
        }

        editor.windowManager.open({
            title: 'Contentlet Callout',
            body: [
                { type: 'textbox', name: 'searchbox', id: "searchbox", label: 'Search', value: '', onkeyup: updateText },
                { type: 'textbox', name: 'contentlet_id', id: "contentletid", label: 'Contentlet ID', value: contentletId },
                { type: "label", text: "Enter the ID of the contentlet you want to use in this box, or use the searchbox to find the ID" },
            ],
            onsubmit: function (e) {
                var calloutAttrs = {};
                calloutAttrs[ATTR_NAME] = e.data.contentlet_id;
                calloutAttrs.class = 'canr-callout-' + e.data.contentlet_id + ' canr-callout';
                calloutAttrs.style = 'height: 0;';
                var calloutElement = editor.dom.createHTML('div', calloutAttrs);
                editor.insertContent(calloutElement + '<script id="canr-callout-script" src="https://www.canr.msu.edu/callout?id=' + e.data.contentlet_id + '"></script>');
            }
        });
        windowID = (editor.windowManager.getWindows()[0]._id);
        var windowStyle = document.createElement('style');
        var windowStyles = document.createTextNode(`
                #displayresultsbox { 
                    background: white; 
                    border-top: 1px solid #c5c5c5; 
                } 
                #displayresultsbox ul{ 
                    max-height: 200px; 
                    overflow-y: auto; 
                    overflow-x: hidden; 
                    padding: 0 20px; 
                } 
                #displayresultsbox ul li{ 
                    overflow: auto;
                    padding: 5px; 
                    word-break: normal; 
                    white-space: normal; 
                    cursor: pointer; 
                    display: grid;
                    grid-template-columns: 60% 15% 25%;              
                } 
                #displayresultsbox ul li:hover{ 
                    text-decoration: none; 
                    color: #fff; 
                    background-color: #0081c2; 
                    background-image: -moz-linear-gradient(top,#08c,#0077b3); 
                    background-image: -webkit-gradient(linear,0 0,0 100%,from(#08c),to(#0077b3)); 
                    background-image: -webkit-linear-gradient(top,#08c,#0077b3); 
                    background-image: -o-linear-gradient(top,#08c,#0077b3); 
                    background-image: linear-gradient(to bottom,#08c,#0077b3); 
                    background-repeat: repeat-x;
                }
                #displayresultsbox ul li:hover span{
                    color: #fff; 
                } 
                #displayresultsbox ul li span {
                    float: right;
                    font-size: 10px;
                    margin-left: 20px;
                }
                #displayresultsbox ul li span{
                    text-align: right;
                }`);
        windowStyle.appendChild(windowStyles);
        var windowElement = document.getElementById(windowID);
        windowElement.insertBefore(windowStyle, windowElement.childNodes[0]);
    }
    function updateResults(response) {
        var resultsbox = document.getElementById('displayresultsbox');
        if (!!resultsbox) {
            resultsbox.parentNode.removeChild(resultsbox);
        }
        resultsbox = document.createElement('div');
        resultsbox.setAttribute("id", "displayresultsbox");
        while (resultsbox.hasChildNodes()) {
            resultsbox.removeChild(resultsbox.firstChild);
        }
        var resultslist = document.createElement('ul');
        resultsbox.appendChild(resultslist);

        response.contentlets.forEach(function (contentlet, index) {
            var contentletli = document.createElement('li');
            contentletli.setAttribute('data-identifier', contentlet.identifier);
            contentletli.setAttribute('data-type', contentlet.contentType);
            contentletli.setAttribute('data-foldername', contentlet.folderName);
            contentletli.setAttribute('data-path', contentlet.folderPath);
            contentletli.innerHTML = contentlet.title + '<span>' + contentlet.contentType + '</span><span>' + contentlet.folderPath + '</span>';
            contentletli.addEventListener('click', insertContentletId);
            resultslist.appendChild(contentletli);
        })
        var window = document.getElementById(windowID + "-body");
        window.insertAdjacentElement("afterend", resultsbox);


    }
    function insertContentletId(e) {
        var contentIDbox = document.getElementById('contentletid');
        contentIDbox.value = e.target.dataset.identifier;
    }

    updateText = debounce(function (attrs) {
        if (busy) {
            console.log('busy');
        } else {
            busy = true;
            searchContentAPI(attrs.target.value).then((response) => {
                updateResults(response);
                busy = false;
                console.log("ok I'm ready");
            })
        }
    }, 250);

    async function searchContentAPI(terms){
        if(terms.trim().length > 0){
            let response = await fetch(host + '/api/canrtools/calloutsearch/' + terms);
            let data = await response.json();
            return data;
        } else {
            return JSON.parse('{"contentlets":[]}');
        }
    }


    // Returns a function, that, as long as it continues to be invoked, will not
    // be triggered. The function will be called after it stops being called for
    // N milliseconds. If `immediate` is passed, trigger the function on the
    // leading edge, instead of the trailing.
    function debounce(func, wait, immediate) {
        var timeout;
        return function () {
            var context = this, args = arguments;
            var later = function () {
                timeout = null;
                if (!immediate) func.apply(context, args);
            };
            var callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow) func.apply(context, args);
        };
    };
    // Add a button that opens a window
    editor.addButton('contentletcallout', {
        icon: 'browse',
        tooltip: 'Insert Contentlet Callout',
        onclick: openInsertCalloutWindow,
        stateSelector: 'div.canr-callout'
    });

    // Adds a menu item to the tools menu
    editor.addMenuItem('contentletcallout', {
        text: 'Contentlet Callout',
        tooltip: 'Insert Contentlet Callout',
        onclick: openInsertCalloutWindow,
        context: 'insert',
    });

});
