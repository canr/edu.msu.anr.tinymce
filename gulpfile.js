const gulp = require('gulp');
const rename = require('gulp-rename');
const terser = require('gulp-terser');
const zip = require('gulp-zip');
const bump = require('gulp-bump');
var fs = require('fs');
var del = require('del');
var versionType = 'patch';

var getPackageJson = function () {
    return JSON.parse(fs.readFileSync('./package.json', 'utf8'));
  };
gulp.task('clean', ()=>{
    return del('bundle/**/*');
})
gulp.task('minify', (f)=>{
    return gulp.src(['./tiny_mce/**/*.js','!./tiny_mce/**/*.min.js'])
        .pipe(rename(function(f){
            f.basename+='.min';
        }))
        .pipe(terser())
        .pipe(gulp.dest(function(f){
            return f.base;
        }));
});
gulp.task('bump-manifest', () =>{
    return gulp.src('./MANIFEST.MF')
        .pipe(bump({type: versionType}))
        .pipe(gulp.dest('./'));
});
gulp.task('bump-package', () =>{
    return gulp.src('./package.json')
        .pipe(bump({type: versionType}))
        .pipe(gulp.dest('./'));
});
gulp.task('bump-package-lock', () =>{
    return gulp.src('./package-lock.json')
        .pipe(bump({type: versionType}))
        .pipe(gulp.dest('./'));
});
gulp.task('move', ()=>{
    return gulp.src(['**/.*','**/*.*','!node_modules/**','!.git/**','!.idea/**','!*.js','!*.json','!.gitignore'])
        .pipe(gulp.dest('bundle/edu.msu.anr.tinymce/'));
});
gulp.task('zip', (f)=>{
    var json = getPackageJson();
    del('release/*');
    return gulp.src(['bundle/**/*.*'])
        .pipe(zip('edu.msu.anr.tinymce.'+json.version+'.zip'))
        .pipe(gulp.dest('release/'));
});
gulp.task('bundle', gulp.series('minify',gulp.parallel('bump-manifest', 'bump-package', 'bump-package-lock'),'move','zip','clean'));
gulp.task('bundle-noclean', gulp.series('minify',gulp.parallel('bump-manifest', 'bump-package', 'bump-package-lock'),'move','zip'));

gulp.task('watch', function(cb){
    gulp.watch(['./tiny_mce/**/*.js','!./tiny_mce/**/*.min.js'], minify);
    cb();
});