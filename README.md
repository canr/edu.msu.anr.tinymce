This plugin configures the TinyMCE installation bundled with dotCMS.

# Important Files
* jsp/tiny_mce_config.jsp - Programmatically defines the TinyMCE configuration JSON object.
* conf/dotmarketing-config-ext.properties - Defines the deployed location of jsp/tiny_mce_config.jsp

# TinyMCE Plugins
TinyMCE has its own plugin system separate from the dotCMS plugin system. dotCMS bundles all of the first-party TinyMCE plugins available with the TinyMCE package. In order to load third-party TinyMCE plugins, place them in this dotCMS plugin's tiny_mce subdirectory.

The following TinyMCE plugins are installed:
* youtube2 - Defines a YouTube button which embeds YouTube videos in the content field.
* contentletcallout - Defines a brief form which adds a Contentlet callout div to the content field.
* table2 - Adds a dialog for adding D3 class and attributes to a chart, as well as changing the top row to headings
* image2 - Adds the ability to add captions to images
* blockquote - Adds dialog for adding blockquotes and citations