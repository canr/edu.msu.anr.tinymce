<%-- 
Use this file to configure TinyMCE plugins.
The plugins must be placed in the tinymce directory on your plugin.

You need to 

Please be aware that only one dotCMS plugin can provide the TinyMCE plugins config file (this file).
The first plugin  according to the normal plugin loading order rules will be the one whose config file is used.

Place a section like this on your web-ext.xml to have dotCMS use your config (remove hello.world with your plugin name):

<servlet>
  <servlet-name>hello.world.tiny_mce_config</servlet-name>
  <servlet-class>org.apache.jsp.plugins.hello.world.tiny_005fmce_005fconfig_jsp</servlet-class>
</servlet>

<servlet-mapping>
  <servlet-name>hello.world.tiny_mce_config</servlet-name>
  <url-pattern>/html/portlet/ext/contentlet/field/tiny_mce_config.jsp</url-pattern>
</servlet-mapping>
  
Once plugins are deployed to the server, they will will be prefixed with plugin_XXXX_ (where XXXX is your plugin name).
Therefore your config must use this name.  Also, you need to make sure the TinyMCE plugin is registering itself with the correct name.
This normally happens in editor_plugin.js in this line:

tinymce.PluginManager.add('myplugin', tinymce.plugins.TinyMCECssExtras);

Which shuold be changed to read:

tinymce.PluginManager.add('plugin_hello.world_myplugin', tinymce.plugins.TinyMCECssExtras);

Make sure your plugin has no - character in its name. Use _ instead.

--%>

var tinyMCEProps = {

    selector: "textarea",
    statusbar: true,
    resize : "both",

    browser_spellcheck: true,

    paste_auto_cleanup_on_paste : true,

    paste_strip_class_attributes : "all",
    convert_urls : true,
    cleanup : true,

    //urlconverter_callback : cmsURLConverter,
    verify_css_classes : false,
    content_css: 'https://cloud.typography.com/7054272/7063992/css/fonts.css,https://www.canr.msu.edu/framework/css/vendor.min.css,https://www.canr.msu.edu/framework/css/main.min.css',

    trim_span_elements : false,
    apply_source_formatting : true,
    relative_urls : false,
    document_base_url : "/",
    file_picker_callback: function(callback, value, meta) {
        cmsFileBrowser(callback, value, meta);
    },
    block_formats: "Paragraph=p;Address=address;Pre=pre;Heading 3=h3;Heading 4=h4",
    image_advtab: true,

    // image_caption is not valid in this version of tinyMCE but will be supported if it ever gets updated
    image_caption: true,
    image_class_list: [
        {title: 'None', value: ''},
        {title: 'Left', value: 'left'},
        {title: 'Right', value: 'right'},
    ],
    link_class_list: [
      {title: 'None', value: ''},
      {title: 'Button', value: 'button'}
    ],

    paste_as_text : false,

    menubar : false,

    theme : 'modern',

    plugins : ''
        + 'advlist,'
        + 'anchor,'
        + 'autolink,'
        + 'charmap,'
        + 'code,'
        + 'colorpicker,'
        + 'compat3x,'
        + 'directionality,'
        + 'dotimageclipboard,'
        + 'emoticons,'
        + 'fullscreen,'
        + 'hr,'
        + 'insertdatetime,'
        + 'lists,'
        + 'link,'
        + 'media,'
        + 'nonbreaking,'
        + 'pagebreak,'
        + 'paste,'
        + 'preview,'
        + 'print,'
        + 'save,'
        + 'searchreplace,'
        + 'template,'
        + 'textcolor,'
        + 'textpattern,'
        + 'validation,'
        + 'visualblocks,'
        + 'visualchars,'
        + 'wordcount,'

        + 'plugin_edu.msu.anr.tinymce_contentletcallout,'
        + 'plugin_edu.msu.anr.tinymce_youtube2,'
        + 'plugin_edu.msu.anr.tinymce_image2,'
        + 'plugin_edu.msu.anr.tinymce_blockquote,'
        + 'plugin_edu.msu.anr.tinymce_table2',

    toolbar1 : 'undo,redo,|,cut,copy,paste,pastetext,|,bold,italic,underline,strikethrough,|,alignleft,aligncenter,alignright,alignjustify,|,bullist,numlist,indent,outdent',
    toolbar2 : 'formatselect,blockquote,removeformat,|,hr,table,charmap,|,link,unlink,anchor,image2,youtube2,contentletcallout,|,searchreplace,code,fullscreen',

    plugin_insertdate_dateFormat : '%Y-%m-%d',
    plugin_insertdate_timeFormat : '%H:%M:%S',

    paste_use_dialog : true,

    width : 900,

};